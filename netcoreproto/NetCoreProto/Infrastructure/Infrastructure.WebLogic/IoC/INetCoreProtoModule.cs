﻿using Autofac.Core;

namespace IBS.NetCoreProto.Infrastructure.WebLogic.IoC
{
    public interface INetCoreProtoModule : IModule
    {
        string FullName { get; }
    }
}
