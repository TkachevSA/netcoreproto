﻿using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands
{
    public class CreateEntityCommandBase<T> : UnitOfWorkScopedCommand<T>
        where T : class, IHasId
    {
        public CreateEntityCommandBase(IScope<IUnitOfWork> unitOfWorkScope)
            : base(unitOfWorkScope)
        {
        }

        public override void Execute(T context)
        {
            UnitOfWorkScope.Instance.Save(context);
            UnitOfWorkScope.Instance.Commit();
        }
    }
}