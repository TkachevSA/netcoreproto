﻿using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands
{
    public abstract class UnitOfWorkScopedCommand : ICommand
    {
        public abstract void Execute();
    }

    public abstract class UnitOfWorkScopedCommand<T> : ICommand<T>
    {
        protected UnitOfWorkScopedCommand(IScope<IUnitOfWork> unitOfWorkScope)
        {
            UnitOfWorkScope = unitOfWorkScope;
        }

        public IScope<IUnitOfWork> UnitOfWorkScope { get; }

        public abstract void Execute(T context);
    }
}