﻿using System;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.Extensions;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands.Handlers
{
    public class CreateOrUpdateEntityHandler<TKey, TCommand, TEntity> : IHandler<TCommand, TKey>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
        where TEntity : class, IHasId<TKey>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrUpdateEntityHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public TKey Handle(TCommand command)
        {
            var id = (command as IHasId)?.Id;
            var entity = id != null && id.Equals(default(TKey)) == false
                ? _mapper.Map(command, _unitOfWork.Find<TEntity>(id))
                : _mapper.Map<TEntity>(command);

            if (entity.IsNew())
                _unitOfWork.Save(entity);

            _unitOfWork.Commit();
            return entity.Id;
        }
    }
}