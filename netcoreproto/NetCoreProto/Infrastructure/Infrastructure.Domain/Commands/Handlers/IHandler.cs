﻿using System.Threading.Tasks;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands.Handlers
{   
    public interface IHandler<in TInput>
    {
        void Handle(TInput input);
    }
    
    public interface IHandler<in TInput, out TOutput>
    {
        TOutput Handle(TInput command);
    }
 
    public interface IAsyncHandler<in TInput>: IHandler<TInput, Task>
    {
    }
    
    public interface IAsyncHandler<in TInput, TOutput>: IHandler<TInput, Task<TOutput>>
    {
    }
}
