﻿using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands
{
    public class DeleteEntityCommandBase<T> : UnitOfWorkScopedCommand<T>
        where T : class, IHasId
    {
        public DeleteEntityCommandBase(IScope<IUnitOfWork> unitOfWorkScope)
            : base(unitOfWorkScope)
        {
        }

        public override void Execute(T context)
        {
            UnitOfWorkScope.Instance.Delete(context);
            UnitOfWorkScope.Instance.Commit();
        }
    }
}