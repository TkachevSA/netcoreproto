﻿namespace IBS.NetCoreProto.Infrastructure.Domain.Commands
{
    public interface ICommand
    {
        void Execute();
    }


    public interface ICommand<in T>
    {
        void Execute(T context);
    }
}