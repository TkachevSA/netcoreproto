﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications;
using IBS.NetCoreProto.Infrastructure.Domain.Extensions;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries
{
    public class ProjectionQuery<TSource, TDest> : IQuery<ILinqSpecification<TDest>, IEnumerable<TDest>>,
        IQuery<ILinqSpecification<TDest>, int>,
        IQuery<Expression<Func<TDest, bool>>, IEnumerable<TDest>>,
        IQuery<Expression<Func<TDest, bool>>, int>,
        IQuery<ExpressionSpecification<TDest>, IEnumerable<TDest>>,
        IQuery<ExpressionSpecification<TDest>, int>
        where TSource : class, IHasId
        where TDest : class, IHasId
    {
        protected readonly ILinqProvider LinqProvider;
        protected readonly IMapper Mapper;
        protected readonly IProjector Projector;

        public ProjectionQuery(ILinqProvider linqProvider, IProjector projector, IMapper mapper)
        {
            Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            LinqProvider = linqProvider ?? throw new ArgumentNullException(nameof(linqProvider));
            Projector = projector ?? throw new ArgumentNullException(nameof(projector));
        }

        IEnumerable<TDest> IQuery<Expression<Func<TDest, bool>>, IEnumerable<TDest>>.Ask(
            Expression<Func<TDest, bool>> spec)
        {
            return Query(spec).ToArray();
        }

        int IQuery<Expression<Func<TDest, bool>>, int>.Ask(Expression<Func<TDest, bool>> spec)
        {
            return CountQuery(spec).Count();
        }

        IEnumerable<TDest> IQuery<ExpressionSpecification<TDest>, IEnumerable<TDest>>.Ask(
            ExpressionSpecification<TDest> spec)
        {
            return Query(spec).ToArray();
        }

        int IQuery<ExpressionSpecification<TDest>, int>.Ask(ExpressionSpecification<TDest> spec)
        {
            return CountQuery(spec).Count();
        }

        IEnumerable<TDest> IQuery<ILinqSpecification<TDest>, IEnumerable<TDest>>.Ask(ILinqSpecification<TDest> spec)
        {
            return Query(spec).ToArray();
        }

        int IQuery<ILinqSpecification<TDest>, int>.Ask(ILinqSpecification<TDest> spec)
        {
            return CountQuery(spec).Count();
        }

        protected virtual IQueryable<TDest> Query(object spec)
        {
            return LinqProvider
                .Query<TSource>()
                .ApplyProjectApplyAgain<TSource, TDest>(Projector, spec);
        }

        protected virtual IQueryable<TDest> CountQuery(object spec)
        {
            return LinqProvider
                .Query<TSource>()
                .ApplyProjectApplyAgainWithoutOrderBy<TSource, TDest>(Projector, spec);
        }
    }
}