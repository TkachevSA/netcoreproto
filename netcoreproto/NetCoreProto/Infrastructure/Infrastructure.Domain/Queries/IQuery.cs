﻿using System.Threading.Tasks;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries
{
    public interface IQuery<out TOutput>
    {
        TOutput Ask();
    }

    public interface IQuery<in TSpecification, out TOutput>
    {
        TOutput Ask(TSpecification spec);
    }

    public interface IAsyncQuery<TOutput> : IQuery<Task<TOutput>>
    {
    }

    public interface IAsyncQuery<in TSpecification, TOutput> : IQuery<TSpecification, Task<TOutput>>
    {
    }

    //public interface IQuery<TEntity, in TSpecification>
    //    where TEntity : class, IHasId
    //    where TSpecification : ISpecification<TEntity>
    //{
    //    IQuery<TEntity, TSpecification> Where(TSpecification specification);

    //    IQuery<TEntity, TSpecification> OrderBy<TProperty>(
    //        Expression<Func<TEntity, TProperty>> expression,
    //        SortOrder sortOrder = SortOrder.Asc);

    //    IQuery<TEntity, TSpecification> Include<TProperty>(Expression<Func<TEntity, TProperty>> expression);


    //    TEntity Single();


    //    TEntity FirstOrDefault();


    //    IEnumerable<TEntity> All();

    //    IPagedEnumerable<TEntity> Paged(int pageNumber, int take);

    //    long Count();
    //}
}
