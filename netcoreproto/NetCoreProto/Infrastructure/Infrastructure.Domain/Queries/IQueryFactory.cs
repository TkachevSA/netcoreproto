﻿using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries
{
    public interface IQueryFactory
    {
        IQuery<TEntity, IExpressionSpecification<TEntity>> GetQuery<TEntity>()
            where TEntity : class, IHasId;

        IQuery<TEntity, TSpecification> GetQuery<TEntity, TSpecification>()
            where TEntity : class, IHasId
            where TSpecification : ISpecification<TEntity>;

        TQuery GetQuery<TEntity, TSpecification, TQuery>()
            where TEntity : class, IHasId
            where TSpecification : ISpecification<TEntity>
            where TQuery : IQuery<TEntity, TSpecification>;
    }
}
