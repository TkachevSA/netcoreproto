﻿using System.Linq;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination
{
    public interface ILinqOrderBy<T>
    {
        IOrderedQueryable<T> Apply(IQueryable<T> queryable);
    }
}