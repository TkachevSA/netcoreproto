﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination
{
    public abstract class Paging<TEntity, TOrderKey> : IPaging<TEntity, TOrderKey>
        where TEntity : class
    {
        // ReSharper disable once StaticMemberInGenericType
        public static int DefaultStartPage = 1;

        // ReSharper disable once StaticMemberInGenericType
        public static int DefaultTake = 30;

        private int _page;

        private int _take;

        protected Paging(int page, int take, params Sorting<TEntity, TOrderKey>[] orderBy)
        {
            Page = page;
            Take = take;

            OrderBy = orderBy ?? throw new ArgumentException("OrderBy can't be null", nameof(orderBy));
        }

        protected Paging()
        {
            Page = DefaultStartPage;
            Take = DefaultTake;
            // ReSharper disable once VirtualMemberCallInConstructor
            OrderBy = BuildDefaultSorting();

            if (OrderBy == null || !OrderBy.Any())
                throw new ArgumentException("OrderBy can't be null or empty", nameof(OrderBy));
        }

        public int Page
        {
            get => _page;
            set
            {
                if (value <= 0)
                    throw new ArgumentException("page must be >= 0", nameof(value));

                _page = value;
            }
        }

        public int Take
        {
            get => _take;
            set
            {
                if (value <= 0)
                    throw new ArgumentException("take must be > 0", nameof(value));

                _take = value;
            }
        }

        public IEnumerable<Sorting<TEntity, TOrderKey>> OrderBy { get; }

        protected abstract IEnumerable<Sorting<TEntity, TOrderKey>> BuildDefaultSorting();
    }
}