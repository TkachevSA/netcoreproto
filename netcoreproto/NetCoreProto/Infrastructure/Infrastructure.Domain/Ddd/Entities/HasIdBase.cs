﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities
{
    public abstract class HasIdBase<TKey> : IHasId<TKey>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
    {
        [Required]
        public virtual TKey Id { get; set; }

        object IHasId.Id => Id;
    }
}