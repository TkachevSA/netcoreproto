﻿using System;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications
{
    public class HasIdSpecification<TKey, T> : ExpressionSpecification<T>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
        where T : class, IHasId<TKey>
    {
        public HasIdSpecification(TKey id)
            : base(x => x.Id.Equals(id))
        {
            Id = id;
        }

        public TKey Id { get; }
    }
}