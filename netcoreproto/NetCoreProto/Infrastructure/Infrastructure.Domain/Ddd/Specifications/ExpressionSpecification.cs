﻿using System;
using System.Linq.Expressions;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.Extensions;

namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications
{
    public class ExpressionSpecification<T> : IExpressionSpecification<T>
        where T : class, IHasId
    {
        public ExpressionSpecification(Expression<Func<T, bool>> expression)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
        }

        private Func<T, bool> Func => this.AsFunc(Expression);
        public Expression<Func<T, bool>> Expression { get; }

        public bool IsSatisfiedBy(T o)
        {
            return Func(o);
        }
    }
}