﻿using System.Linq;

namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications
{
    public interface ILinqSpecification<T>
        where T : class
    {
        IQueryable<T> Apply(IQueryable<T> query);
    }
}