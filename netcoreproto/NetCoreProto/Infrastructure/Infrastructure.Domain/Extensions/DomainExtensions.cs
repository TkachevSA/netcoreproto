﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.Extensions
{
    public static class DomainExtensions
    {
        private static readonly ConcurrentDictionary<Expression, object> _cachedFunctions
            = new ConcurrentDictionary<Expression, object>();

        public static Func<TEntity, bool> AsFunc<TEntity>(this object entity, Expression<Func<TEntity, bool>> expr)
            where TEntity : class, IHasId
        {
            //@see http://sergeyteplyakov.blogspot.ru/2015/06/lazy-trick-with-concurrentdictionary.html
            return (Func<TEntity, bool>) _cachedFunctions.GetOrAdd(expr, id => new Lazy<object>(
                () => _cachedFunctions.GetOrAdd(id, expr.Compile())));
        }

        public static bool Is<TEntity>(this TEntity entity, Expression<Func<TEntity, bool>> expr)
            where TEntity : class, IHasId
        {
            return AsFunc(entity, expr).Invoke(entity);
        }

        //public static IQuery<TEntity, IExpressionSpecification<TEntity>> Where<TEntity>(
        //    this IQuery<TEntity, IExpressionSpecification<TEntity>> query,
        //    ExpressionSpecification<TEntity> expression)
        //    where TEntity : class, IHasId
        //{
        //    return query
        //        .Where(new ExpressionSpecification<TEntity>(AsFunc(TEntity, expression)));
        //}
    }
}
