﻿namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    public enum MapType : byte
    {
        Any = 0,
        Expression = 1,
        Func = 2
    }
}