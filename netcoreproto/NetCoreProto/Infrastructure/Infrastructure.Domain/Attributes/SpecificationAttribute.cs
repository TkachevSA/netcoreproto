﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    public class SpecificationAttribute : Attribute, ITypeAssociation
    {
        public Type EntityType { get; }

        public SpecificationAttribute(Type entityType)
        {
            EntityType = entityType;
        }
    }
}
