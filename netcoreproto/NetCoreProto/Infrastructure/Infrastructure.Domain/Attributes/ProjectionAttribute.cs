﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ProjectionAttribute : Attribute, ITypeAssociation
    {
        public Type EntityType { get; }

        public MapType MapType { get; }

        public ProjectionAttribute(Type entityType, MapType mapType = MapType.Any)
        {
            EntityType = entityType ?? throw new ArgumentNullException(nameof(entityType));
            MapType = mapType;
        }
    }
}
