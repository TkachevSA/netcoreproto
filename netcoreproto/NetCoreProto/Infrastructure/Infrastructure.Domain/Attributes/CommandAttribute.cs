﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    public class CommandAttribute : MessageAttribute
    {
        public CommandAttribute(Type entityType) : base(entityType)
        {
        }
    }
}