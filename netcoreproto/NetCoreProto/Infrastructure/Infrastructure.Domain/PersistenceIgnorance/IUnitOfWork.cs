﻿using System;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();

        void Save<TEntity>(TEntity entity)
            where TEntity : class, IHasId;

        void Delete<TEntity>(TEntity entity)
            where TEntity : class, IHasId;

        TEntity Find<TEntity>(object id)
            where TEntity : class, IHasId;

        IHasId Find(Type entityType, object id);
    }
}