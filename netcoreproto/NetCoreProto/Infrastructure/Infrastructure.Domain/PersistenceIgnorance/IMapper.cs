﻿namespace IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance
{
    public interface IMapper
    {
        TReturn Map<TReturn>(object src);

        TReturn Map<TReturn>(object src, TReturn dest);
    }
}