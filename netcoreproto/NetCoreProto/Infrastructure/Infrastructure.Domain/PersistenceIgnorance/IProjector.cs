﻿using System.Linq;

namespace IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance
{
    public interface IProjector
    {
        IQueryable<TReturn> Project<TReturn>(IQueryable queryable);
    }
}