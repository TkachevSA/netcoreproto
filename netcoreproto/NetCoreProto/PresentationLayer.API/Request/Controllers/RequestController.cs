﻿using System;
using System.Collections.Generic;
using AutoMapper;
using IBS.NetCoreProto.PresentationLayer.API.Request.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace IBS.NetCoreProto.PresentationLayer.API.Request.Controllers
{
    [Route("api/[controller]")]
    public class RequestController : Controller
    {
        private readonly IRequestQueryService _requestQueryService;
        private readonly IMapper _mapper;

        [Obsolete("Only for DI, don't use it in your code", true)]
        public RequestController()
        {
        }

        public RequestController(IRequestQueryService requestQueryService, IMapper mapper)
        {
            _requestQueryService = requestQueryService;
            _mapper = mapper;
        }

        public IEnumerable<RequestViewModel> GetAll()
        {
            var response = _requestQueryService.GetAllRequests();
            var result = _mapper.Map<IEnumerable<RequestViewModel>>(response);

            return result;
        }

        [HttpGet("{id}", Name = "GetById")]
        public IActionResult GetById(string id)
        {
            var request = new ByIdRequest(Guid.NewGuid());
            var response = _requestQueryService.GetRequestById(request);


            if (response == null)
                return NotFound();

            var result = _mapper.Map<RequestViewModel>(response);

            return new ObjectResult(result);
        }
    }
}