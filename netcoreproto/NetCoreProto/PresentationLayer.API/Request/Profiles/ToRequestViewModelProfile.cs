﻿using AutoMapper;
using IBS.NetCoreProto.PresentationLayer.API.Request.ViewModels;

namespace IBS.NetCoreProto.PresentationLayer.API.Request.Profiles
{
    public class ToRequestViewModelProfile : Profile
    {
        public ToRequestViewModelProfile()
        {
            CreateMap<RequestViewModel, RequestDto>();
        }
    }
}
