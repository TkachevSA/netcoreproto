﻿namespace IBS.NetCoreProto.PresentationLayer.API.Request.ViewModels
{
    public class RequestViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Processed { get; set; }
    }
}