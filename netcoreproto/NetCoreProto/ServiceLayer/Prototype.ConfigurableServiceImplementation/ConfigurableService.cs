﻿using IBS.NetCoreProto.ServicesLayer.Prototype.ConfigurableServiceDescription;

namespace IBS.NetCoreProto.ServicesLayer.Prototype.ConfigurableServiceImplementation
{
    public class ConfigurableService : IConfigurableService
    {
        public string GetConfigurableText(string param)
        {
            return $"Hello {param}";
        }
    }
}
