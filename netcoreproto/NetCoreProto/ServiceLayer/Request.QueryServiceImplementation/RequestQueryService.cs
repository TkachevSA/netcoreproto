﻿using System.Collections.Generic;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Dtos;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Requests;

namespace IBS.NetCoreProto.ServiceLayer.Request.QueryServiceImplementation
{
    public class RequestQueryService : IRequestQueryService
    {
        public IEnumerable<RequestDto> GetAllRequests()
        {
            var response = new List<RequestDto>
            {
                new RequestDto
                {
                    Id = 1,
                    Name = "Simple request with number 1",
                    Processed = false
                },
                new RequestDto
                {
                    Id = 2,
                    Name = "Simple request with number 2",
                    Processed = false
                }
            };

            return response;
        }

        public RequestDto GetRequestById(ByIdRequest request)
        {
            var response = new RequestDto
            {
                Id = 2,
                Name = "Simple request with number 2",
                Processed = false
            };

            return response;
        }

        public IEnumerable<VehicleDto> GetVehicleListForRequests(ByIdRequest request)
        {   

            var response = new List<VehicleDto>
            {
                new VehicleDto
                {
                    Id = 101,
                    Vendor = "VOLVO",
                    Model = "FH 16 750",
                    PlateNumber = "o234oo174RU"
                },
                new VehicleDto
                {
                    Id = 102,
                    Vendor = "Scania",
                    Model = "S730 V8",
                    PlateNumber = "x456xx174RU"
                }
            };

            return response;
        }
    }
}
