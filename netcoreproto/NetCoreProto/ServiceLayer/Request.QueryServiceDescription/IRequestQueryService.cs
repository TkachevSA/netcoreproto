﻿using System.Collections.Generic;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Dtos;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Requests;

namespace IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription
{
    public interface IRequestQueryService
    {
        IEnumerable<RequestDto> GetAllRequests();

        RequestDto GetRequestById(ByIdRequest request);

        IEnumerable<VehicleDto> GetVehicleListForRequests(ByIdRequest request);
    }
}
