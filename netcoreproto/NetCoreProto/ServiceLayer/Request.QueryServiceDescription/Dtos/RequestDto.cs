﻿namespace IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Dtos
{
    public class RequestDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Processed { get; set; }
    }
}
