﻿namespace IBS.NetCoreProto.ServicesLayer.Prototype.ConfigurableServiceDescription
{
    public interface IConfigurableService
    {
        string GetConfigurableText(string param);
    }
}
