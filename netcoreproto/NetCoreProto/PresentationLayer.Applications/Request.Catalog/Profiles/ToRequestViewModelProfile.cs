﻿using IBS.NetCoreProto.PresentationLayer.Applications.Request.Catalog.Models;

namespace IBS.NetCoreProto.PresentationLayer.Applications.Request.Catalog.Profiles
{
    public class ToRequestViewModelProfile : Profile
    {
        public ToRequestViewModelProfile()
        {
            CreateMap<RequestViewModel, RequestDto>();
        }
    }
}
