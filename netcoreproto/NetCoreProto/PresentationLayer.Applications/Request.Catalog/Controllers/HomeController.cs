﻿using IBS.NetCoreProto.PresentationLayer.Applications.Request.Catalog.Models;

namespace IBS.NetCoreProto.PresentationLayer.Applications.Request.Catalog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRequestQueryService _requestQueryService;
        private readonly IMapper _mapper;

        public HomeController(IRequestQueryService requestQueryService, IMapper mapper)
        {
            _requestQueryService = requestQueryService;
            _mapper = mapper;
        }
        public IActionResult Index()
        {
            var request = new ByIdRequest(Guid.NewGuid());
            var response = _requestQueryService.GetRequestById(request);

            var model = _mapper.Map<RequestViewModel>(response);

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
