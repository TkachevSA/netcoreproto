namespace IBS.NetCoreProto.PresentationLayer.Applications.Request.Catalog.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}