﻿namespace IBS.NetCoreProto.PresentationLayer.Applications.Request.Catalog
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("./Configs/Configuration.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = configurationBuilder.Build();
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAutoMapper();

            RegisterViewComponets(services);

            var builder = new ContainerBuilder();
            builder.Populate(services);

            var mainConfigurationModule = new ConfigurationModule(Configuration);
            builder.RegisterModule(mainConfigurationModule);
            
            //builder.RegisterType<RequestOtherQueryService>().As<IRequestQueryService>();

            ApplicationContainer = builder.Build();

           return  new AutofacServiceProvider(ApplicationContainer);
        }

        private static void RegisterViewComponets(IServiceCollection services)
        {
            var myAssembly = typeof(VehicleListViewComponent).Assembly;
            services.AddMvc().AddApplicationPart(myAssembly);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.FileProviders.Add(new EmbeddedFileProvider(myAssembly, "VehicleList"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
