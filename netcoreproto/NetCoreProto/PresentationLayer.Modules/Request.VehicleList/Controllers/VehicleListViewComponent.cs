﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using IBS.NetCoreProto.PresentationLayer.Modules.Request.VehicleList.ViewModels;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace IBS.NetCoreProto.PresentationLayer.Modules.Request.VehicleList.Controllers
{   
    [ViewComponent(Name = "VehicleList")]
    public class VehicleListViewComponent : ViewComponent
    {
        private readonly IRequestQueryService _requestQueryService;
        private readonly IMapper _mapper;

        public VehicleListViewComponent(IRequestQueryService requestQueryService, IMapper mapper)
        {
            _requestQueryService = requestQueryService;
            _mapper = mapper;
        }

        public async Task<ViewViewComponentResult> InvokeAsync(int requestId)
        {
            var request = new ByIdRequest(Guid.NewGuid());
            var response = _requestQueryService.GetVehicleListForRequests(request);

            var result = _mapper.Map<IEnumerable<VehicleViewModel>>(response);

            return View(result);
        }
    }
}
