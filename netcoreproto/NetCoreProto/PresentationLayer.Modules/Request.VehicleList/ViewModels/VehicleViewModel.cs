﻿namespace IBS.NetCoreProto.PresentationLayer.Modules.Request.VehicleList.ViewModels
{
    public class VehicleViewModel
    {
        public int Id { get; set; }
        public string Vendor { get; set; }
        public string Model { get; set; }
        public string PlateNumber { get; set; }
    }
}
