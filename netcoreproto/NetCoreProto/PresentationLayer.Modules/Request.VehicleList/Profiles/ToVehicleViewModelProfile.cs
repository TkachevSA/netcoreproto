﻿using AutoMapper;
using IBS.NetCoreProto.PresentationLayer.Modules.Request.VehicleList.ViewModels;
using IBS.NetCoreProto.ServiceLayer.Request.QueryServiceDescription.Dtos;

namespace IBS.NetCoreProto.PresentationLayer.Modules.Request.VehicleList.Profiles
{
    public class ToVehicleViewModelProfile : Profile
    {
        public ToVehicleViewModelProfile()
        {
            CreateMap<VehicleViewModel, VehicleDto>();
        }
    }
}
