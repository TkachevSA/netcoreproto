﻿namespace IBS.NetCoreProto.PresentationLayer.Modules.Prototype.ConfigurableViewComponent.ViewModels
{
    public class ConfigurableModel
    {
        private readonly string _modelText;
        private string _textFromService;

        public ConfigurableModel()
        {
            _modelText = "This is a simple text";
        }

        public string ModelText => _modelText;

        public string TextFromService => _textFromService;
    }
}
 