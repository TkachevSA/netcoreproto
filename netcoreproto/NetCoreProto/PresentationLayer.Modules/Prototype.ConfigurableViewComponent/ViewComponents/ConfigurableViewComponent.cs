﻿using IBS.NetCoreProto.ServicesLayer.Prototype.ConfigurableServiceDescription;
using Microsoft.AspNetCore.Mvc;

namespace IBS.NetCoreProto.PresentationLayer.Modules.Prototype.ConfigurableViewComponent.ViewComponents
{
    public class ConfigurableViewComponent : ViewComponent
    {
        private readonly IConfigurableService _configurableService;

        public ConfigurableViewComponent(IConfigurableService configurableService)
        {
            _configurableService = configurableService;
        }

        public IViewComponentResult Invoke(string constPart)
        {
            var content = _configurableService.GetConfigurableText(constPart);
            return View(content);
        }
    }
}
