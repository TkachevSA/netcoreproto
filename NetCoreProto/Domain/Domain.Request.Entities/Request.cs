﻿using System;
using System.Linq.Expressions;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Domain.Request.Entities
{
    public class Request : IHasId
    {
        [Obsolete("Only for model binders and EF, don't use it in your code", true)]
        internal Request()
        {
        }

        public static Expression<Func<Request, bool>> ProcessedRule = x => x.Processed;

        public Request(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        private string _name;

        public int Id { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(nameof(Name));
                }

                _name = value;
            }
        }

        public bool Processed { get; set; }

        object IHasId.Id => Id;

        //bool IsProcessed()
        //{
        //    this.Is(Processed);
        //}
    }
}
