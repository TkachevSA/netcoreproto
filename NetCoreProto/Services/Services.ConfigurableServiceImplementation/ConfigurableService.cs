﻿using IBS.NetCoreProto.Services.ConfigurableServiceDescription;

namespace IBS.NetCoreProto.Services.ConfigurableServiceImplementation
{
    public class ConfigurableService : IConfigurableService
    {
        public string GetConfigurableText(string param)
        {
            return $"Hello {param}";
        }
    }
}
