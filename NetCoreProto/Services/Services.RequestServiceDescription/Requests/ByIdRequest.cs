﻿using System;

namespace IBS.NetCoreProto.Services.RequestServiceDescription.Requests
{
    public class ByIdRequest
    {
        public ByIdRequest(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
