﻿namespace IBS.NetCoreProto.Services.RequestServiceDescription.Dtos
{
    public class RequestDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Processed { get; set; }
    }
}
