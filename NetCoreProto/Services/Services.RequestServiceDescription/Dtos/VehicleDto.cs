﻿namespace IBS.NetCoreProto.Services.RequestServiceDescription.Dtos
{
    public class VehicleDto
    {
        public int Id { get; set; }
        public string Vendor { get; set; }
        public string Model { get; set; }
        public string PlateNumber { get; set; }
    }
}
