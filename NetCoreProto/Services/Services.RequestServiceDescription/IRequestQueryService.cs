﻿using System.Collections.Generic;
using IBS.NetCoreProto.Services.RequestServiceDescription.Dtos;
using IBS.NetCoreProto.Services.RequestServiceDescription.Requests;

namespace IBS.NetCoreProto.Services.RequestServiceDescription
{
    public interface IRequestQueryService
    {
        IEnumerable<RequestDto> GetAllRequests();

        RequestDto GetRequestById(ByIdRequest request);

        IEnumerable<VehicleDto> GetVehicleListForRequests(ByIdRequest request);
    }
}
