﻿namespace IBS.NetCoreProto.Services.ConfigurableServiceDescription
{
    public interface IConfigurableService
    {
        string GetConfigurableText(string param);
    }
}
