﻿using AutoMapper;
using IBS.NetCoreProto.Applications.Request.Catalog.Models;
using IBS.NetCoreProto.Services.RequestServiceDescription.Dtos;

namespace IBS.NetCoreProto.Applications.Request.Catalog.Profiles
{
    public class ToRequestViewModelProfile : Profile
    {
        public ToRequestViewModelProfile()
        {
            CreateMap<RequestViewModel, RequestDto>();
        }
    }
}
