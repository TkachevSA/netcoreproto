﻿namespace IBS.NetCoreProto.Applications.Request.Catalog.Models
{
    public class RequestViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Processed { get; set; }
    }
}
