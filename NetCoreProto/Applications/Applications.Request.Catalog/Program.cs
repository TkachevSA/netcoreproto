﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace IBS.NetCoreProto.Applications.Request.Catalog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
