﻿using System;
using System.Diagnostics;
using AutoMapper;
using IBS.NetCoreProto.Applications.Request.Catalog.Models;
using IBS.NetCoreProto.Services.RequestServiceDescription;
using IBS.NetCoreProto.Services.RequestServiceDescription.Requests;
using Microsoft.AspNetCore.Mvc;

namespace IBS.NetCoreProto.Applications.Request.Catalog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRequestQueryService _requestQueryService;
        private readonly IMapper _mapper;

        public HomeController(IRequestQueryService requestQueryService, IMapper mapper)
        {
            _requestQueryService = requestQueryService;
            _mapper = mapper;
        }
        public IActionResult Index()
        {
            var request = new ByIdRequest(Guid.NewGuid());
            var response = _requestQueryService.GetRequestById(request);

            var model = _mapper.Map<RequestViewModel>(response);

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
