﻿using System;
using System.Diagnostics;
using IBS.NetCoreProto.Infrastructure.WebLogic.IoC;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using ErrorViewModel = IBS.NetCoreProto.Applications.FrontEnd.Models.ErrorViewModel;

namespace IBS.NetCoreProto.Applications.FrontEnd.Controllers
{
    public class HomeController : Controller
    {
        private readonly IServiceProvider _serviceProvider;

        public HomeController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            var modules = _serviceProvider.GetServices<INetCoreProtoModule>();
            return View(modules);
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
