﻿using AutoMapper;
using IBS.NetCoreProto.API.Request.ViewModels;
using IBS.NetCoreProto.Services.RequestServiceDescription.Dtos;

namespace IBS.NetCoreProto.API.Request.Profiles
{
    public class ToRequestViewModelProfile : Profile
    {
        public ToRequestViewModelProfile()
        {
            CreateMap<RequestViewModel, RequestDto>();
        }
    }
}
