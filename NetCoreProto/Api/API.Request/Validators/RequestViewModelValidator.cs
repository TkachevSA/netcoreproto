﻿using FluentValidation;
using IBS.NetCoreProto.API.Request.ViewModels;

namespace IBS.NetCoreProto.API.Request.Validators
{
    public class RequestViewModelValidator : AbstractValidator<RequestViewModel>
    {
        public RequestViewModelValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Name).Length(0, 10);
            RuleFor(x => x.Processed).NotNull();
        }
    }
}