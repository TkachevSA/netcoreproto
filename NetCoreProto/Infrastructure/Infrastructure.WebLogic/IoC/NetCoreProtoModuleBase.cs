﻿using Autofac;

namespace IBS.NetCoreProto.Infrastructure.WebLogic.IoC
{
    public abstract class NetCoreProtoModuleBase : Module, INetCoreProtoModule
    {
        public virtual string FullName => GetType().Assembly.FullName;

    }
}
