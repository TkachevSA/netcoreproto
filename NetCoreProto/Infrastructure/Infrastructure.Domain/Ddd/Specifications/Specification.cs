﻿using System.Linq;

namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications
{
    /// <summary>
    /// https://nblumhardt.com/archives/implementing-the-specification-pattern-via-linq/
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Specification<T>
    {
        public bool IsSatisfiedBy(T item)
        {
            return SatisfyingElementsFrom(new[] { item }.AsQueryable()).Any();
        }

        public abstract IQueryable<T> SatisfyingElementsFrom(IQueryable<T> candidates);
    }
}
