﻿namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications
{
    public interface ISpecification<in T>
    {
        bool IsSatisfiedBy(T o);
    }
}
