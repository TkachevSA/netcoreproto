﻿using System;
using System.Linq.Expressions;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications
{
    public interface IExpressionSpecification<T> : ISpecification<T>
        where T : class, IHasId
    {
        Expression<Func<T, bool>> Expression { get; }
    }
}
