﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands
{
    public interface IScope<out T> : IDisposable
    {
        T Instance { get; }
    }
}