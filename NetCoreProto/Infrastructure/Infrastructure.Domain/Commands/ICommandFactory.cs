﻿using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands
{
    public interface ICommandFactory
    {
        TCommand GetCommand<TEntity, TCommand>()
            where TCommand : ICommand<TEntity>;

        T GetCommand<T>()
            where T : ICommand;

        CreateEntityCommandBase<T> GetCreateCommand<T>()
            where T : class, IHasId;

        DeleteEntityCommandBase<T> GetDeleteCommand<T>()
            where T : class, IHasId;
    }
}
