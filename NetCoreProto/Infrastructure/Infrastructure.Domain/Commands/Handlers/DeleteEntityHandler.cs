﻿using System;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Commands.Handlers
{
    public class DeleteEntityHandler<TKey, TEntity> : IHandler<TKey>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
        where TEntity : class, IHasId<TKey>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteEntityHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public void Handle(TKey key)
        {
            var entity = _unitOfWork.Find<TEntity>(key);
            if (entity == null)
                throw new ArgumentException($"Entity {typeof(TEntity).Name} with id={key} doesn't exists");

            _unitOfWork.Delete(entity);
            _unitOfWork.Commit();
        }
    }
}