﻿using System;
using System.Linq;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance
{
    public interface ILinqProvider
    {
        IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IHasId;


        IQueryable Query(Type t);
    }
}