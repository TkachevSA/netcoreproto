﻿using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;
using IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries
{
    public class PagedQuery<TSource, TDest, TSortKey> : ProjectionQuery<TSource, TDest>,
        IQuery<IPaging<TDest, TSortKey>, IPagedEnumerable<TDest>>
        where TSource : class, IHasId
        where TDest : class, IHasId
    {
        public PagedQuery(ILinqProvider linqProvider, IProjector projector, IMapper mapper)
            : base(linqProvider, projector, mapper)
        {
        }

        public IPagedEnumerable<TDest> Ask(IPaging<TDest, TSortKey> spec)
        {
            return Query(spec).ToPagedEnumerable(spec);
        }
    }


    public class PagedQuery<TSource, TDest> : PagedQuery<TSource, TDest, int>, IQuery<IPaging, IPagedEnumerable<TDest>>
        where TSource : class, IHasId
        where TDest : class, IHasId
    {
        public PagedQuery(ILinqProvider linqProvider, IProjector projector, IMapper mapper) : base(linqProvider,
            projector, mapper)
        {
        }

        public IPagedEnumerable<TDest> Ask(IPaging spec)
        {
            return Query(spec).ToPagedEnumerable(spec);
        }
    }
}