﻿using System;
using System.Linq;
using IBS.NetCoreProto.Infrastructure.Domain.Attributes;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.Extensions;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries
{
    public class GetByIdQuery<TKey, TEntity, TProjection> : IQuery<TKey, TProjection>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
        where TEntity : class, IHasId<TKey>
    {
        protected readonly ILinqProvider LinqProvider;
        protected readonly IMapper Mapper;
        protected readonly IProjector Projector;

        public GetByIdQuery(ILinqProvider linqProvider, IProjector projector, IMapper mapper)
        {
            LinqProvider = linqProvider ?? throw new ArgumentNullException(nameof(linqProvider));
            Projector = projector ?? throw new ArgumentNullException(nameof(projector));
            Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public virtual TProjection Ask(TKey specification)
        {
            return typeof(TProjection).GetMapType() != MapType.Func
                ? Query(specification).Project<TProjection>(Projector).SingleOrDefault()
                : Mapper.Map<TProjection>(Query(specification).SingleOrDefault());
        }

        private IQueryable<TEntity> Query(TKey specification)
        {
            return LinqProvider
                .Query<TEntity>()
                .Where(x => specification.Equals(x.Id));
        }
    }
}