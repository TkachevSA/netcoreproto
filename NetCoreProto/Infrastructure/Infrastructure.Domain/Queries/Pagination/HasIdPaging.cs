﻿using System;
using System.Collections.Generic;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination
{
    public class HasIdPaging<TEntity> : HasIdPaging<TEntity, int>
        where TEntity : class, IHasId<int>
    {
        public HasIdPaging(int page, int take)
            : base(page, take)
        {
        }

        public HasIdPaging()
        {
        }
    }

    public class HasIdPaging<TEntity, TKey> : Paging<TEntity, TKey>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
        where TEntity : class, IHasId<TKey>
    {
        public HasIdPaging(int page, int take)
            : base(page, take, new Sorting<TEntity, TKey>(x => x.Id, SortOrder.Desc))
        {
        }

        public HasIdPaging()
        {
        }

        protected override IEnumerable<Sorting<TEntity, TKey>> BuildDefaultSorting()
        {
            yield return new Sorting<TEntity, TKey>(x => x.Id, SortOrder.Desc);
        }
    }
}