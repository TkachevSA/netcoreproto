﻿using System.Collections.Generic;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination
{
    public interface IPagedEnumerable<out T> : IEnumerable<T>
    {
        long TotalCount { get; }
    }
}
