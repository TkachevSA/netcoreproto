﻿using System;
using System.Linq.Expressions;

namespace IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination
{
    public enum SortOrder
    {
        Asc = 1,
        Desc = 2
    }

    public class Sorting<TEntity, TKey>
        where TEntity : class
    {
        public Sorting(
            Expression<Func<TEntity, TKey>> expression,
            SortOrder sortOrder = SortOrder.Asc)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
            SortOrder = sortOrder;
        }

        public Expression<Func<TEntity, TKey>> Expression { get; }

        public SortOrder SortOrder { get; }
    }
}