﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    interface ITypeAssociation
    {
        Type EntityType { get; }
    }
}
