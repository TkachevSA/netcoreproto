﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    public class DomainEventAttribute : MessageAttribute
    {
        public DomainEventAttribute(Type entityType) : base(entityType)
        {
        }
    }
}