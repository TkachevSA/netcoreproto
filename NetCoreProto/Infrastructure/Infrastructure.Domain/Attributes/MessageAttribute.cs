﻿using System;

namespace IBS.NetCoreProto.Infrastructure.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public abstract class MessageAttribute : Attribute, ITypeAssociation
    {
        public Type EntityType { get; }

        protected MessageAttribute(Type entityType)
        {
            EntityType = entityType ?? throw new ArgumentNullException(nameof(entityType));
        }
    }
}
