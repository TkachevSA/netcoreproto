﻿using System;
using System.Threading.Tasks;
using IBS.NetCoreProto.Infrastructure.Domain.Commands.Handlers;
using IBS.NetCoreProto.Infrastructure.Domain.Queries;

namespace IBS.NetCoreProto.Infrastructure.Domain.Extensions
{
    public static class FunctionalExtensions
    {
        public static Func<TSource, TResult> Compose<TSource, TIntermediate, TResult>(
            this Func<TSource, TIntermediate> func1, Func<TIntermediate, TResult> func2)
        {
            return x => func2(func1(x));
        }

        public static TResult PipeTo<TSource, TResult>(
            this TSource source, Func<TSource, TResult> func)
        {
            return func(source);
        }

        public static T PipeToIf<T>(this T source
            , Func<T, bool> condition
            , Func<T, T> evaluator)
            where T : class
        {
            return condition(source)
                ? evaluator(source)
                : source;
        }

        public static T PipeToIfNotNull<T>(this T source, Func<object, T> evaluator)
            where T : class
        {
            return PipeToIf(source, x => x != null, x => evaluator(x));
        }

        public static TOutput Either<TInput, TOutput>(this TInput o
            , Func<TInput, bool> condition
            , Func<TInput, TOutput> ifTrue
            , Func<TInput, TOutput> ifFalse)
            where TInput : class
        {
            return condition(o) ? ifTrue(o) : ifFalse(o);
        }

        public static T Do<T>(T obj, Action<T> action)
        {
            action(obj);
            return obj;
        }

        #region Cqrs

        public static TResult PipeTo<TSource, TResult>(
            this TSource source, IQuery<TSource, TResult> query)
        {
            return query.Ask(source);
        }

        public static Task<TResult> PipeTo<TSource, TResult>(
            this TSource source, IQuery<TSource, Task<TResult>> query)
        {
            return query.Ask(source);
        }

        public static TResult PipeTo<TSource, TResult>(
            this TSource source, IHandler<TSource, TResult> query)
        {
            return query.Handle(source);
        }

        public static TSource PipeTo<TSource>(
            this TSource source, IHandler<TSource> query)
        {
            query.Handle(source);
            return source;
        }

        public static TOutput Ask<TInput, TOutput>(this IQuery<TInput, TOutput> query)
            where TInput : new()
        {
            return query.Ask(new TInput());
        }

        public static Func<TIn, TOut> ToFunc<TIn, TOut>(this IQuery<TIn, TOut> query)
        {
            return query.Ask;
        }

        public static Func<TIn, TOut> ToFunc<TIn, TOut>(this IHandler<TIn, TOut> handler)
        {
            return handler.Handle;
        }

        #endregion
    }
}