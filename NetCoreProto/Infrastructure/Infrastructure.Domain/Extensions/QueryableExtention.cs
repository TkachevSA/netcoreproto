﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Entities;
using IBS.NetCoreProto.Infrastructure.Domain.Ddd.Specifications;
using IBS.NetCoreProto.Infrastructure.Domain.PersistenceIgnorance;
using IBS.NetCoreProto.Infrastructure.Domain.Queries.Pagination;

namespace IBS.NetCoreProto.Infrastructure.Domain.Extensions
{
    public static class QueryableExtention
    {
        public static IQueryable<T> OrderIf<T, TKey>(this IQueryable<T> query, bool condition, bool acs,
            Expression<Func<T, TKey>> keySelector)
        {
            if (!condition)
                return query;

            return acs
                ? query.OrderBy(keySelector)
                : query.OrderByDescending(keySelector);
        }

        public static IQueryable<T> OrderThenIf<T, TKey>(this IQueryable<T> query, bool condition, bool acs,
            Expression<Func<T, TKey>> keySelector)
        {
            if (!condition)
                return query;
            return acs
                ? ((IOrderedQueryable<T>) query).ThenBy(keySelector)
                : ((IOrderedQueryable<T>) query).ThenByDescending(keySelector);
        }

        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition,
            Expression<Func<T, bool>> predicate)
        {
            return condition ? query.Where(predicate) : query;
        }

        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition,
            Expression<Func<T, bool>> predicateIfTrue, Expression<Func<T, bool>> predicateIfFalse)
        {
            return condition
                ? query.Where(predicateIfTrue)
                : query.Where(predicateIfFalse);
        }


        public static bool In<T>(this T value, params T[] values)
        {
            return values != null && values.Contains(value);
        }

        public static bool In<T>(this T value, IQueryable<T> values)
        {
            return values != null && values.Contains(value);
        }

        public static bool In<T>(this T value, IEnumerable<T> values)
        {
            return values != null && values.Contains(value);
        }

        public static bool NotIn<T>(this T value, params T[] values)
        {
            return values == null || !values.Contains(value);
        }

        public static bool NotIn<T>(this T value, IQueryable<T> values)
        {
            return values == null || !values.Contains(value);
        }

        public static bool NotIn<T>(this T value, IEnumerable<T> values)
        {
            return values == null || !values.Contains(value);
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> source, ILinqSpecification<T> spec)
            where T : class
        {
            return spec.Apply(source);
        }

        public static IQueryable<T> MaybeOrderBy<T>(this IQueryable<T> source, object sort)
        {
            return sort is ILinqOrderBy<T> srt
                ? srt.Apply(source)
                : source;
        }

        public static IQueryable<TDest> ApplyProjectApplyAgain<TSource, TDest>(this IQueryable<TSource> queryable,
            IProjector projector, object spec)
            where TSource : class, IHasId
            where TDest : class, IHasId
        {
            return queryable
                .MaybeWhere(spec)
                .MaybeOrderBy(spec)
                .Project<TDest>(projector)
                .MaybeWhere(spec)
                .MaybeOrderBy(spec);
        }

        public static IQueryable<TDest> ApplyProjectApplyAgainWithoutOrderBy<TSource, TDest>(
            this IQueryable<TSource> queryable, IProjector projector, object spec)
            where TSource : class, IHasId
            where TDest : class, IHasId
        {
            return queryable
                .MaybeWhere(spec)
                .Project<TDest>(projector)
                .MaybeWhere(spec);
        }

        public static IQueryable<T> MaybeWhere<T>(this IQueryable<T> source, object spec)
            where T : class, IHasId
        {
            if (spec is ILinqSpecification<T> specification)
                source = specification.Apply(source);

            if (spec is Expression<Func<T, bool>> expr)
                source = source.Where(expr);

            if (spec is ExpressionSpecification<T> exprSpec)
                source = source.Where(exprSpec.Expression);
            return source;
        }

        public static IQueryable<TDest> Project<TDest>(this IQueryable source, IProjector projector)
        {
            return projector.Project<TDest>(source);
        }

        public static TEntity ById<TEntity>(this ILinqProvider linqProvider, int id)
            where TEntity : class, IHasId<int>
        {
            return linqProvider.Query<TEntity>().ById(id);
        }

        public static TEntity ById<TEntity>(this IQueryable<TEntity> queryable, int id)
            where TEntity : class, IHasId<int>
        {
            return queryable.SingleOrDefault(x => x.Id == id);
        }
    }
}