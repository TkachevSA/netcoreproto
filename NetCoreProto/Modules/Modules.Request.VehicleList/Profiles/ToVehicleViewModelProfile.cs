﻿using AutoMapper;
using IBS.NetCoreProto.Modules.Request.VehicleList.ViewModels;
using IBS.NetCoreProto.Services.RequestServiceDescription.Dtos;

namespace IBS.NetCoreProto.Modules.Request.VehicleList.Profiles
{
    public class ToVehicleViewModelProfile : Profile
    {
        public ToVehicleViewModelProfile()
        {
            CreateMap<VehicleViewModel, VehicleDto>();
        }
    }
}
