﻿using IBS.NetCoreProto.Services.ConfigurableServiceDescription;
using Microsoft.AspNetCore.Mvc;

namespace IBS.NetCoreProto.FrontEnd.ConfigurableViewComponent.ViewComponents
{
    public class ConfigurableViewComponent : ViewComponent
    {
        private readonly IConfigurableService _configurableService;

        public ConfigurableViewComponent(IConfigurableService configurableService)
        {
            _configurableService = configurableService;
        }

        public IViewComponentResult Invoke(string constPart)
        {
            var content = _configurableService.GetConfigurableText(constPart);
            return View(content);
        }
    }
}
