﻿using Autofac;
using IBS.NetCoreProto.Infrastructure.WebLogic.IoC;

namespace IBS.NetCoreProto.FrontEnd.ConfigurableViewComponent
{
    public class SomeModule : NetCoreProtoModuleBase
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SomeModule>().As<INetCoreProtoModule>();
        }
    }
}
